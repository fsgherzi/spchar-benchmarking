//
// Created by Francesco Sgherzi on 23/08/21.
//
#pragma once

#include <KokkosKernels_config.h>
#include <KokkosKernels_IOUtils.hpp>
#include <KokkosSparse_spadd.hpp>
#include <KokkosSparse_spgemm.hpp>
#include <KokkosSparse_spmv.hpp>
#include <Kokkos_Core.hpp>
#include <chrono>
#include <cmath>
#include <random>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <limits>
#include <unordered_map>
#include <vector>

#include "types.h"
#include "perf_profiler.h"
#if defined(USE_ARMPL)
#include "armpl.h"
#endif
#include "utils.h"

template <typename kernel_handler_type, MMComputationType computation_type>
void spgemm_inner(
    kernel_handler_type kh, 
    const matrix_type& matrix_a,
    const matrix_type& matrix_b,
    matrix_type& matrix_c
    ) {
  if (computation_type == MMComputationType::Symbolic) {
    KokkosSparse::spgemm_symbolic(kh, matrix_a, false, matrix_b, false, matrix_c);
  } else {
    KokkosSparse::spgemm_numeric(kh, matrix_a, false, matrix_b, false, matrix_c);
  }
  Kokkos::fence();
}
template <MMComputationType computation_type>
void spadd_inner(
    KernelHandle* kh, 
    const matrix_type& matrix_a,
    const matrix_type& matrix_b,
    matrix_type& matrix_c
    ) {
  if (computation_type == MMComputationType::Symbolic){
    // Symbolic Execution
    KokkosSparse::spadd_symbolic(kh, matrix_a, matrix_b, matrix_c);
  } else {
    const Scalar alpha = 1.0;
    const Scalar beta = 1.0;
    KokkosSparse::spadd_numeric(kh, alpha, matrix_a, beta, matrix_b, matrix_c);
  }
  Kokkos::fence();
}

template <typename x_type, typename h_x_type, typename layout, typename y_type>
void spmv_inner(
    x_type x, 
    h_x_type h_x, 
    y_type y, 
    layout mode, 
    u32 num_runs, 
    u32 warmup_runs, 
    matrix_type matrix 
    ) {
  const auto alpha = 1.0;
  const auto beta  = 0.0;

  Kokkos::deep_copy(x, h_x);

  for (u32 i = 0; i < warmup_runs; ++i) {
    KokkosSparse::spmv(&mode, alpha, matrix, x, beta, y);
  }
  for (u32 i = 0; i < num_runs; ++i) {
    IProfiler::get_instance()->profile_begin(100); 
    KokkosSparse::spmv(&mode, 1.0, matrix, x, beta, y);
    Kokkos::fence();
    IProfiler::get_instance()->profile_end();
  }
}

void test_spmv(
    matrix_type matrix, 
    u32 num_runs,
    u32 warmup_runs,
    bool debug
    ) {
  using mv_type = Kokkos::View<Scalar*, Kokkos::LayoutRight>;
  using h_mv_type = typename mv_type::HostMirror;

  Ordinal num_rows = 0;
  Ordinal num_cols = 0;

  const auto mode = 'N';

  if (debug) std::cout << "[INFO] testing SpMV" << std::endl;

  num_rows = matrix.numRows();
  num_cols = matrix.numCols();
  mv_type x("X", num_cols);
  mv_type y("Y", num_rows);
  h_mv_type h_x = Kokkos::create_mirror_view(x);

  for (i32 i = 0; i < num_cols; ++i) {
    h_x(i) = (Scalar)(1.0 * (rand() % 40) - 20.);
  }

  spmv_inner(x, h_x, y, mode, num_runs, warmup_runs, matrix);
}

template <enum MMComputationType computation_type>
void test_spgemm(
    matrix_type matrix, 
    u32 num_runs,
    u32 warmup_runs,
    bool debug
    ) {

  if(debug) std::cout << "[INFO] testing SpGEMM" << std::endl;

  // Do shallow copy here... Otherwise we would get too much memory wasted
  matrix_type matrix_b = matrix_type(matrix), matrix_c;

  KernelHandle kh;
  kh.set_dynamic_scheduling(true);
  kh.set_verbose(false);
  kh.set_team_work_size(256);
  kh.create_spgemm_handle(KokkosSparse::SPGEMMAlgorithm::SPGEMM_KK_SPEED);

  for (u32 i = 0; i < warmup_runs; ++i) {
    spgemm_inner<KernelHandle, MMComputationType::Symbolic>(kh, matrix, matrix_b, matrix_c);
    if(computation_type == MMComputationType::Numeric) spgemm_inner<KernelHandle, MMComputationType::Numeric>(kh, matrix, matrix_b, matrix_c);
  }
  for (u32 i = 0; i < num_runs; ++i) {
    if (computation_type == MMComputationType::Symbolic){
      IProfiler::get_instance()->profile_begin(100);
      spgemm_inner<KernelHandle, MMComputationType::Symbolic>(kh, matrix, matrix_b, matrix_c);
      IProfiler::get_instance()->profile_end();
    } else {
      spgemm_inner<KernelHandle, MMComputationType::Symbolic>(kh, matrix, matrix_b, matrix_c);
      IProfiler::get_instance()->profile_begin(100);
      spgemm_inner<KernelHandle, MMComputationType::Numeric>(kh, matrix, matrix_b, matrix_c);
      IProfiler::get_instance()->profile_end();
    }
  }


  kh.destroy_spgemm_handle();
}

template <MMComputationType computation_type>
void test_spadd(
    matrix_type matrix,
    u32 num_runs,
    u32 warmup_runs, 
    bool debug 
    ) {

  if(debug) std::cout << "[INFO] testing SpADD" << std::endl;


  matrix_type out_matrix;
  // Do deee copy here, we need to change the B matrix
  matrix_type input_matrix_b = matrix_type("matrix_b", matrix);

  generate_perturbation(input_matrix_b, debug);

  KernelHandle kh;
  kh.set_dynamic_scheduling(true);
  kh.set_verbose(false);
  kh.set_team_work_size(256);
  kh.create_spadd_handle(true);

  for (u32 i = 0; i < warmup_runs; ++i) {
    spadd_inner<MMComputationType::Symbolic>(&kh, matrix, input_matrix_b, out_matrix);
    if(computation_type == MMComputationType::Symbolic) spadd_inner<MMComputationType::Numeric>(&kh, matrix, input_matrix_b, out_matrix);
  }
  for (u32 i = 0; i < num_runs; ++i) {
    if (computation_type == MMComputationType::Symbolic){
      IProfiler::get_instance()->profile_begin(100);
      spadd_inner<MMComputationType::Symbolic>(&kh, matrix, input_matrix_b, out_matrix);
      IProfiler::get_instance()->profile_end();
    } else {
      spadd_inner<MMComputationType::Symbolic>(&kh, matrix, input_matrix_b, out_matrix);
      IProfiler::get_instance()->profile_begin(100);
      spadd_inner<MMComputationType::Numeric>(&kh, matrix, input_matrix_b, out_matrix);
      IProfiler::get_instance()->profile_end();
    }
  }
  kh.destroy_spadd_handle();
}

#if defined(USE_ARMPL)
void armpl_test_spmv(matrix_type& kokkos_matrix, u32 runs, u32 warmup_runs, bool debug) {
  auto matrix            = as_armpl_sparse_matrix(kokkos_matrix, debug);
  const armpl_uint_t N   = kokkos_matrix.numRows();

  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix, ARMPL_SPARSE_HINT_SPMV_OPERATION, ARMPL_SPARSE_OPERATION_NOTRANS), "armpl_spmat_hint", debug);
  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix, ARMPL_SPARSE_HINT_SPMV_INVOCATIONS, ARMPL_SPARSE_INVOCATIONS_MANY), "armpl_spmat_hint", debug);
  ARMPL_CHECK_ERR(armpl_spmv_optimize(matrix), "armpl_spmv_optimize", debug);

  f32* x = (f32*) std::malloc(sizeof(f32*) * N); 
  f32* y = (f32*) std::malloc(sizeof(f32*) * N); 

  for (u32 i = 0; i < N; ++i) {
    x[i] = (1.0 * (rand() % 40) - 20.0f);
  }


  for(u32 i = 0; i < warmup_runs; ++i) {
    ARMPL_CHECK_ERR(armpl_spmv_exec_s(ARMPL_SPARSE_OPERATION_NOTRANS, 1.0f, matrix, x, 0.0f, y), "armpl_spmv_exec_s", debug);
  }

  for(u32 i = 0; i < runs; ++i){
    IProfiler::get_instance()->profile_begin(100);
    armpl_spmv_exec_s(ARMPL_SPARSE_OPERATION_NOTRANS, 1.0f, matrix, x, 0.0f, y);
    IProfiler::get_instance()->profile_end();
  }
  ARMPL_CHECK_ERR(armpl_spmat_destroy(matrix), "armpl_spmat_destroy", debug);

  std::free(x);
  std::free(y);
}

void armpl_test_spadd(matrix_type& kokkos_matrix_a, u32 runs, u32 warmup_runs, bool debug){

  // Create lhs armpl matrix
  auto matrix_a          = as_armpl_sparse_matrix(kokkos_matrix_a, debug);
  const armpl_uint_t N   = kokkos_matrix_a.numRows();
  const armpl_uint_t M   = kokkos_matrix_a.numCols();

  // Create rhs armpl matrix
  auto kokkos_matrix_b = matrix_type("matrix_b", kokkos_matrix_a);
  generate_perturbation(kokkos_matrix_b, debug);
  auto matrix_b        = as_armpl_sparse_matrix(kokkos_matrix_b, debug);

  // Hints for the matrix
  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_a, ARMPL_SPARSE_HINT_SPADD_OPERATION, ARMPL_SPARSE_OPERATION_NOTRANS), "armpl_spmat_hint", debug);
  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_a, ARMPL_SPARSE_HINT_SPADD_INVOCATIONS, ARMPL_SPARSE_INVOCATIONS_MANY), "armpl_spmat_hint", debug);

  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_b, ARMPL_SPARSE_HINT_SPADD_OPERATION, ARMPL_SPARSE_OPERATION_NOTRANS), "armpl_spmat_hint", debug);
  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_b, ARMPL_SPARSE_HINT_SPADD_INVOCATIONS, ARMPL_SPARSE_INVOCATIONS_MANY), "armpl_spmat_hint", debug);

  // Warmup
  for(u32 i = 0; i < warmup_runs; ++i) {
    // Create results matrix
    armpl_matrix_type matrix_c = armpl_spmat_create_null(N, M);
    ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_c, ARMPL_SPARSE_HINT_SPADD_OPERATION, ARMPL_SPARSE_OPERATION_NOTRANS), "armpl_spmat_hint", debug);
    ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_c, ARMPL_SPARSE_HINT_SPADD_INVOCATIONS, ARMPL_SPARSE_INVOCATIONS_SINGLE), "armpl_spmat_hint", debug);

    // Hints for the algorithm
    ARMPL_CHECK_ERR(armpl_spadd_optimize(ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_SCALAR_ONE, matrix_a, ARMPL_SPARSE_SCALAR_ONE, matrix_b, matrix_c), "armpl_spadd_optimize", debug);

    // Run
    ARMPL_CHECK_ERR(armpl_spadd_exec_s(ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_OPERATION_NOTRANS, 1.0f, matrix_a, 1.0f, matrix_b, matrix_c), "armpl_spadd_exec_s", debug);
    ARMPL_CHECK_ERR(armpl_spmat_destroy(matrix_c), "armpl_spmat_destroy", debug);
  }

  // Profile
  for(u32 i = 0; i < runs; ++i) {
    // Same thing as before, the matrix C can be used only once per spadd invocation
    armpl_matrix_type matrix_c = armpl_spmat_create_null(N, M);
    ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_c, ARMPL_SPARSE_HINT_SPADD_OPERATION, ARMPL_SPARSE_OPERATION_NOTRANS), "armpl_spmat_hint", debug);
    ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_c, ARMPL_SPARSE_HINT_SPADD_INVOCATIONS, ARMPL_SPARSE_INVOCATIONS_SINGLE), "armpl_spmat_hint", debug);

    ARMPL_CHECK_ERR(armpl_spadd_optimize(ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_SCALAR_ONE, matrix_a, ARMPL_SPARSE_SCALAR_ONE, matrix_b, matrix_c), "armpl_spadd_optimize", debug);

    IProfiler::get_instance()->profile_begin(100);
    armpl_spadd_exec_s(ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_OPERATION_NOTRANS, 1.0f, matrix_a, 1.0f, matrix_b, matrix_c);
    IProfiler::get_instance()->profile_end();

    ARMPL_CHECK_ERR(armpl_spmat_destroy(matrix_c), "armpl_spmat_destroy", debug);
  }

  ARMPL_CHECK_ERR(armpl_spmat_destroy(matrix_a), "armpl_spmat_destroy", debug);
  ARMPL_CHECK_ERR(armpl_spmat_destroy(matrix_b), "armpl_spmat_destroy", debug);
}


void armpl_test_spgemm(matrix_type& kokkos_matrix_a, u32 runs, u32 warmup_runs, bool debug) {

  // Create lhs armpl matrix
  auto matrix_a          = as_armpl_sparse_matrix(kokkos_matrix_a, debug);
  const armpl_uint_t N   = kokkos_matrix_a.numRows();
  const armpl_uint_t M   = kokkos_matrix_a.numCols();

  // Create rhs armpl matrix
  auto kokkos_matrix_b = matrix_type("matrix_b", kokkos_matrix_a);
  auto matrix_b        = as_armpl_sparse_matrix(kokkos_matrix_b, debug);

  // Hints for the matrix
  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_a, ARMPL_SPARSE_HINT_SPMM_OPERATION, ARMPL_SPARSE_OPERATION_NOTRANS), "armpl_spmat_hint", debug);
  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_a, ARMPL_SPARSE_HINT_SPMM_INVOCATIONS, ARMPL_SPARSE_INVOCATIONS_MANY), "armpl_spmat_hint", debug);

  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_b, ARMPL_SPARSE_HINT_SPMM_OPERATION, ARMPL_SPARSE_OPERATION_NOTRANS), "armpl_spmat_hint", debug);
  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_b, ARMPL_SPARSE_HINT_SPMM_INVOCATIONS, ARMPL_SPARSE_INVOCATIONS_MANY), "armpl_spmat_hint", debug);

  armpl_matrix_type matrix_c = armpl_spmat_create_null(N, M);
  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_c, ARMPL_SPARSE_HINT_SPMM_INVOCATIONS, ARMPL_SPARSE_INVOCATIONS_MANY), "armpl_spmat_hint", debug);

  // Hint to compute the symbolic phase for matrix_c
  ARMPL_CHECK_ERR(armpl_spmat_hint(matrix_c, ARMPL_SPARSE_HINT_SPMM_STRATEGY, ARMPL_SPARSE_SPMM_STRAT_OPT_FULL_STRUCT), "armpl_spmat_hint", debug);

  // Actually compute the symbolic phase for matrix_c
  // Note that this call has a different ordering of arguments than arm_spadd_optimize!!! (for unknown reasons)
  ARMPL_CHECK_ERR(armpl_spmm_optimize(ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_SCALAR_ONE, matrix_a, matrix_b, ARMPL_SPARSE_SCALAR_ONE, matrix_c), "armpl_spmm_optimize", debug);

  for(u32 i = 0; i < warmup_runs; ++i){
    ARMPL_CHECK_ERR(armpl_spmm_exec_s(ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_OPERATION_NOTRANS, 1.0f, matrix_a, matrix_b, 1.0f, matrix_c), "armpl_spmm_exec_s", debug);
  }

  for(u32 i = 0; i < runs; ++i){
    IProfiler::get_instance()->profile_begin(100); 
    armpl_spmm_exec_s(ARMPL_SPARSE_OPERATION_NOTRANS, ARMPL_SPARSE_OPERATION_NOTRANS, 1.0f, matrix_a, matrix_b, 1.0f, matrix_c);
    IProfiler::get_instance()->profile_end();
  }

  ARMPL_CHECK_ERR(armpl_spmat_destroy(matrix_a), "armpl_spmat_destroy", debug);
  ARMPL_CHECK_ERR(armpl_spmat_destroy(matrix_b), "armpl_spmat_destroy", debug);
  ARMPL_CHECK_ERR(armpl_spmat_destroy(matrix_c), "armpl_spmat_destroy", debug);
}
#endif

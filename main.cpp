#include <KokkosKernels_IOUtils.hpp>
#include <KokkosSparse_IOUtils.hpp>
#include <KokkosSparse_CrsMatrix.hpp>
#include <KokkosSparse_spmv.hpp>
#include <Kokkos_Core.hpp>
#include <algorithm>
#include <boost/program_options.hpp>
#include <boost/program_options/variables_map.hpp>
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "utils/test_loader.h"
#include "utils/types.h"
#include "utils/config.h"
#include "utils/perf_profiler.h"

#if defined (ENABLE_PARSEC_HOOKS)
#include "hooks.h"
#endif

namespace po = boost::program_options;

void add_all_options(po::options_description &options) {
  options.add_options()
    ("matrix_paths",po::value<std::vector<std::string>>()->multitoken())
    ("num_runs", po::value<i32>())
    ("debug", po::value<bool>())
    ("kokkos_debug",po::value<bool>())
    ("tests", po::value<std::vector<std::string>>()->multitoken())
    ("warmup_runs",po::value<i32>())
    ("out_file", po::value<std::string>())
    ("profiler_config", po::value<std::string>())
    ("profiler_kind", po::value<std::string>())
    ("threads", po::value<int>());
}

po::variables_map parse_args(const po::options_description &options, i32 argc,
    char **argv) {
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, options), vm);
  po::notify(vm);
  return vm;
}

i32 main(i32 argc, char **argv) {

  Kokkos::initialize(argc, argv);

  const std::vector<std::string> all_tests = {"spmv", "spgemm_symbolic", "spgemm_numeric", "spadd_symbolic", "spadd_numeric"};
  const std::string default_out_file = "./../results/results.csv";
  std::string profiler_config = "", profiler_kind = "any";
  std::vector<std::string> matrix_paths = {}, tests = {};
  u32 num_runs, warmup_runs = 0;
  bool debug = false, kokkos_debug = false;
  po::options_description options;

  add_all_options(options);
  auto options_map = parse_args(options, argc, argv);

  std::fstream results_file;
  auto out_file_options = std::ios::app | std::ios::out;

  if(options_map.count("out_file")){
    results_file.open(options_map["out_file"].as<std::string>(), out_file_options);
  } else{
    results_file.open(default_out_file, out_file_options);
  }

  if (options_map.count("matrix_paths")) {
    matrix_paths = options_map["matrix_paths"].as<std::vector<std::string>>();
  } else {
    std::cerr << "[ERR] Matrix paths were not provided" << std::endl;
    exit(-1);
  }

  if (options_map.count("num_runs")) {
    num_runs = options_map["num_runs"].as<i32>();
  } else {
    num_runs = 20;
  }

  if (options_map.count("debug")) {
    debug = options_map["debug"].as<bool>();
  }

  if(options_map.count("profiler_kind"))
    profiler_kind = options_map["profiler_kind"].as<std::string>();

  if(options_map.count("profiler_config")) {
    profiler_config = options_map["profiler_config"].as<std::string>();
    IProfiler::get_instance()->configure(profiler_config, profiler_kind, SamplingType::SINGLE_SAMPLE, false);
  } else {
    std::cerr << "[ERR] Path to profiler configuration was not provided." << std::endl;
    exit(-1);
  }

  if(options_map.count("kokkos_debug")){
    kokkos_debug = options_map["kokkos_debug"].as<bool>();
  }

  if (options_map.count("tests")) {
    tests = options_map["tests"].as<std::vector<std::string>>();
  } else {
    tests = all_tests;
  }

  if (options_map.count("warmup_runs")) {
    warmup_runs = options_map["warmup_runs"].as<i32>();
  } else {
    warmup_runs = num_runs;
  }

  if (debug) {
    std::cout << "[CONFIGURATION] num_runs: " << num_runs << std::endl;
    std::cout << "[CONFIGURATION] warmup_runs: " << warmup_runs << std::endl;
    std::cout << "[CONFIGURATION] tests: \n";

    for (const auto &test : tests) {
      std::cout << "\t" << test << " ";
    }

    std::cout << std::endl;
  }

  // Begin TESTS
  for (auto matrix_path : matrix_paths) {
    std::vector<std::vector<f64>> results;
    try {
      auto matrix = KokkosSparse::Impl::read_kokkos_crst_matrix<matrix_type>(matrix_path.c_str());
      const auto N = matrix.numRows();
      const auto M = matrix.numCols();
      const auto nnz = matrix.nnz();
      for (const auto &test : tests) {
        // Do the necessary tests
        IProfiler::get_instance()->clear();
        if (test == "spmv") {
#if defined(USE_ARMPL)
          armpl_test_spmv(matrix, num_runs, warmup_runs, kokkos_debug);
#else
          test_spmv(matrix, num_runs, warmup_runs, kokkos_debug);
#endif
          std::cout << "Completed test" << std::endl;
        } else if (test == "spgemm_numeric") {
#if defined(USE_ARMPL)
          armpl_test_spgemm(matrix, num_runs, warmup_runs, kokkos_debug);
#else
          test_spgemm<MMComputationType::Numeric>(matrix, num_runs, warmup_runs, kokkos_debug);
#endif
        } else if (test == "spgemm_symbolic") {
#if defined(USE_ARMPL)
          std::cout << "ARMPL does not expose spgemm_symbolic " << std::endl;
#else
          test_spgemm<MMComputationType::Symbolic>(matrix, num_runs, warmup_runs, kokkos_debug);
#endif
        } else if (test == "spadd_symbolic"){
#if defined(USE_ARMPL)
          std::cout << "ARMPL does not expose spadd_symbolic " << std::endl;
#else
          test_spadd<MMComputationType::Symbolic>(matrix, num_runs, warmup_runs, kokkos_debug);
#endif
        } else if (test == "spadd_numeric"){
#if defined(USE_ARMPL)
          armpl_test_spadd(matrix, num_runs, warmup_runs, kokkos_debug);
#else
          test_spadd<MMComputationType::Numeric>(matrix, num_runs, warmup_runs, kokkos_debug);
#endif
        } else{
          std::cerr << test << " not handled." << std::endl;
        }
        const auto csv_line_beginning = matrix_path + "," + test + "," + std::to_string(N) + "," + std::to_string(M) + "," + std::to_string(nnz) + "," + std::getenv("OMP_NUM_THREADS");
        IProfiler::get_instance()->dump(csv_line_beginning, results_file);
      }
    } catch (std::runtime_error &e) {
      if (debug) std::cerr << e.what() << std::endl;
      continue;
    }
  }

  results_file.close();
  Kokkos::finalize();


  return 0;
}

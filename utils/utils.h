#pragma once 

#include "types.h"
#if defined (USE_ARMPL)
#define ARMPL_CHECK_ERR(ans, message, debug) { armpl_check_error((ans), message,  __FILE__, __LINE__, debug); }
inline void armpl_check_error(armpl_status_t code, std::string message, const char *file, int line, bool debug)
{
   if (code != ARMPL_STATUS_SUCCESS) std::cerr << "[ERR " << file << "@" << line << "] " << message << " code ~> " << code << std::endl; 
   else {
     //if (debug) std::cout << "[" << file << "@" << line << "] " << message << " was successful." << std::endl;   
  }
}
armpl_matrix_type as_armpl_sparse_matrix(const matrix_type& kokkos_matrix, bool debug) {
    armpl_matrix_type armpl_matrix;
    
    const armpl_uint_t N   = kokkos_matrix.numRows();
    const armpl_uint_t M   = kokkos_matrix.numCols();
    const armpl_uint_t nnz = kokkos_matrix.nnz();

    f32* values          = (f32*) std::malloc(sizeof(f32) * nnz);
    armpl_int_t* row_ptr = (armpl_int_t*) std::malloc(sizeof(armpl_int_t) * (N + 1));
    armpl_int_t* col_idx = (armpl_int_t*) std::malloc(sizeof(armpl_int_t) * (nnz));

    for(u32 i = 0; i < N + 1; ++i){
        row_ptr[i] = kokkos_matrix.graph.row_map(i);
    }

    for(u32 i = 0; i < nnz; ++i){
        col_idx[i] = kokkos_matrix.graph.entries(i);
        values[i] = kokkos_matrix.values(i);
    }

    ARMPL_CHECK_ERR(armpl_spmat_create_csr_s(&armpl_matrix, M, N, row_ptr, col_idx, values, 0), "armpl_spmat_create_csr_s", debug);

    return armpl_matrix;
}
#endif

void generate_perturbation(matrix_type& matrix, bool debug){

  auto sampled_perturbations = 0;
  auto actual_perturbations  = 0;
  auto time_seed             = system_clock::now().time_since_epoch().count();
  std::seed_seq ss           = {u32(time_seed & 0xffffffff), u32(time_seed >> 32)};
  auto move_probability      = 2.0f / 3.0f;

  std::random_device device;
  std::mt19937 rng(device());
  std::uniform_real_distribution<f32> udist(0, 1);


  for(i32 row_idx = 0; row_idx < matrix.numRows(); ++row_idx){
    const auto begin = matrix.graph.row_map(row_idx);
    const auto end = matrix.graph.row_map(row_idx + 1);

    const auto col_idx = matrix.graph.entries(row_idx);


    for(i32 row_ptr = begin; row_ptr < end; ++row_ptr){
      auto offset = 0;
      if(udist(rng) <= move_probability){
        auto can_move_before = (col_idx > 0)                      && ((row_ptr == 0)                  || (col_idx > (matrix.graph.entries(row_idx - 1) + 1)));
        auto can_move_after  = (col_idx < (matrix.numCols() - 1)) && ((row_ptr == (matrix.nnz() - 1)) || ((col_idx + 1) < matrix.graph.entries(row_ptr + 1)));

        if(can_move_before && can_move_after) {
          offset = udist(rng) <= 0.5 ? -1 : 1;
        } else if (can_move_before) {
          offset = -1;
        } else if (can_move_after) {
          offset = 1;
        }

        sampled_perturbations++;
        actual_perturbations += can_move_before || can_move_after;

        matrix.graph.entries(row_ptr) += offset;
      }
    }
  }
  if(debug) std::cout << "[INFO] perturbed " << ((f32)actual_perturbations / matrix.nnz()) * 100 << "% (" << ((f32)actual_perturbations / sampled_perturbations) * 100 << "% of possible ones)" << std::endl;
}


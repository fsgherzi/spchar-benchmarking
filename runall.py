import multiprocessing
import os

matrix_dir = os.getenv("MATRIX_DIR")
pkind = "any"
matrices = os.listdir(matrix_dir)
tests = ["spadd_numeric"] #, "spadd_symbolic", "spadd_numeric", "spgemm_symbolic", "spgemm_numeric"] #, "spadd_symbolic", "spadd_numeric", "spgemm_symbolic", "spgemm_numeric"]
cores = [1, 2, 4, 8] #multiprocessing.cpu_count()
for matrix in matrices:
    print(f"doing {matrix}")
    for test in tests:
        for num_cores in cores:
            os.system(f"OMP_PROC_BIND=true OMP_PLACES=threads OMP_NUM_THREADS={num_cores} ./build/kokkos_benchmark --threads={num_cores} --matrix_paths {matrix_dir}/{matrix} --num_runs 5 --warmup_runs 2 --tests {test} --debug 1 --kokkos_debug 1 --out_file ./results/local.csv --profiler_config perf_counters/intel.json --profiler_kind {pkind}")

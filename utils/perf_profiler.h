#pragma once

#include <fstream>
#include <iostream>
#include <linux/perf_event.h>
#include <nlohmann/json.hpp>
#include <string>
#include <sys/ioctl.h>
#include <thread>
#include <chrono>
#include <mutex>

using json = nlohmann::json;
using sys_clock = std::chrono::system_clock;

static constexpr uint32_t MAX_SAMPLES = 12;

typedef struct {
    uint64_t value;
    uint64_t id;
} counter;

typedef struct {
    uint64_t count;
    counter counters[MAX_SAMPLES];
} measurements;

typedef struct {
    uint64_t id;
    counter *reading;
    int fd;
} event_config;

enum ProfilerCategory {
    PERF = 0,
    PARSEC = 1,
    NO_PROFILER = 2
};

enum SamplingType {
    SINGLE_SAMPLE = 0,
    REGULAR = 1
};

template <typename ProfilerType>
class ProfilerInstance {

public:
    static ProfilerType* get_instance() {
        if(instance == nullptr)
            instance = new ProfilerType();

        return instance;
    }
private:
    inline static ProfilerType *instance = nullptr;

};


template <typename TimerType>
class Profiler {
    using profiler_impl_t = Profiler<TimerType>;

    friend profiler_impl_t* ProfilerInstance<profiler_impl_t>::get_instance();
public:
    Profiler(ProfilerCategory type): m_type(type){}
    virtual Profiler<TimerType> &configure(const std::string &, const std::string &, SamplingType, const bool &debug = false) = 0;
    virtual Profiler<TimerType> &dump() = 0;
    virtual Profiler<TimerType> &dump(const std::string&, const std::string&) = 0;
    virtual Profiler<TimerType> &dump(const std::string&, std::fstream&) = 0;
    void set_sampling_type(SamplingType sampling_type){this->m_sampling_criterion = sampling_type;}

    virtual void profile_begin(uint32_t sample_time) = 0;
    virtual void profile_end()  = 0;

private:
    ProfilerCategory m_type;
protected:
    SamplingType m_sampling_criterion;
};

template <typename TimerType>
class PerfProfiler: public Profiler<TimerType> {
    using profiler_impl_t = PerfProfiler<TimerType>;

    friend profiler_impl_t* ProfilerInstance<profiler_impl_t>::get_instance();
public:

    PerfProfiler(const PerfProfiler &) = delete;

    PerfProfiler &configure(const std::string &config_file_path, const std::string &category, SamplingType sampling_criterion, const bool &debug = false) override {
        m_debug = debug;
        this->set_sampling_type(sampling_criterion);
        std::ifstream config_file(config_file_path);
        m_configuration = json::parse(config_file);
        return setup_perf(category);
    }

    void profile_begin(uint32_t sample_time) override {
        samples.emplace_back();
        begin_timer = sys_clock::now();
        ioctl(perf_fd, PERF_EVENT_IOC_RESET, PERF_IOC_FLAG_GROUP);
        ioctl(perf_fd, PERF_EVENT_IOC_ENABLE, PERF_IOC_FLAG_GROUP);
        if(this->m_sampling_criterion == SamplingType::REGULAR) {
            profiler_thread = new std::thread([&]() {
                profile(TimerType(sample_time), perf_fd, run_n);
            });

            // Enable profiler thread, should be waiting for the
            should_run_profiler_thread = true;
        }
    }

    void profile_end() override {
        measurements data;
        auto end_timer = sys_clock::now();
        durations.push_back(std::chrono::duration_cast<std::chrono::nanoseconds>(end_timer - begin_timer).count());

        // Store the final sample
        ioctl(perf_fd, PERF_EVENT_IOC_DISABLE, PERF_IOC_FLAG_GROUP);
        (void) read(perf_fd, &data, sizeof(data));
        samples[run_n].push_back(data);


        if(this->m_sampling_criterion == SamplingType::REGULAR) {
            should_run_profiler_thread = false;
            profiler_thread->join();
            delete profiler_thread;
            profiler_thread = nullptr;
        }

        run_n++;

    }

    void clear() {
        run_n = 0;
        samples.clear();
        durations.clear();
    }


    PerfProfiler &dump() override {

        for (uint32_t run_id = 0; run_id < run_n; ++run_id) {
            const auto &run_samples = samples[run_id];
            for (uint32_t sample_id = 0; sample_id < run_samples.size(); ++sample_id) {
                const auto &sample = run_samples[sample_id];
                for (uint32_t i = 0; i < m_event_names.size(); ++i) {
                    std::cout << "[" << run_id << "] [" << sample_id << "] " << m_event_names[i] << " ~> "
                              << sample.counters[i].value
                              << std::endl;
                }
            }
        }


        return *this;
    }

    PerfProfiler &dump(const std::string &additional_text, const std::string& filename) override {
        std::fstream out_file;
        out_file.open(filename, std::ios::app | std::ios::out);

        if(!out_file.is_open()) {
            throw std::runtime_error("Could not open file " + filename);
        }
        this->dump(additional_text, out_file);
        out_file.close();
        return *this;
    }

    PerfProfiler &dump(const std::string &additional_text, std::fstream &file) override {
        for (uint32_t run_id = 0; run_id < run_n; ++run_id) {
            const auto &run_samples = samples[run_id];
            const auto &elapsed_time = durations[run_id];
            for (uint32_t sample_id = 0; sample_id < run_samples.size(); ++sample_id) {
                const auto &sample = run_samples[sample_id];
                file << additional_text << ","
                     << run_id << ","
                     << sample_id << ","
                     << elapsed_time;
                for (uint32_t i = 0; i < m_event_names.size(); ++i) {
                    file << "," << sample.counters[i].value;
                }

                file << "\n";
            }
        }
        file.flush();

        return *this;


    }

private:

    bool m_debug {false},  should_run_profiler_thread {false};
    uint32_t run_n {0};
    int perf_fd {-1};

    ProfilerCategory m_type;
    measurements m_data;


    json m_configuration;
    sys_clock::time_point begin_timer;
    std::vector<uint64_t> durations;
    std::vector<std::string> m_event_names;
    std::vector<std::vector<measurements>> samples;

    std::thread *profiler_thread;

    PerfProfiler() : Profiler<TimerType>(ProfilerCategory::PERF) {}

    void profile(const TimerType& sleep_time, const int fd, const uint32_t run_id) {
        measurements data;

        while (should_run_profiler_thread) {
            // Wait for timer
            std::this_thread::sleep_for(sleep_time);
            // Disable profiling and read the data
            ioctl(fd, PERF_EVENT_IOC_DISABLE, PERF_IOC_FLAG_GROUP);
            int32_t ret = read(fd, &data, sizeof(data));
            if (ret < 0) throw std::runtime_error("Error: failed to read.");

            // Store the data and reenable profiling
            samples[run_id].push_back(data);

            if(m_debug) std::cout << "Profiler tick!" << std::endl;

            ioctl(fd, PERF_EVENT_IOC_ENABLE, PERF_IOC_FLAG_GROUP);
        }
    }


    PerfProfiler &setup_perf(const std::string &category) {
        for (auto &event: m_configuration["categories"][category]) {
            std::cout << "Configuring " << event["name"] << std::endl;
            if (add_event(event, perf_fd) < 0) {
                std::cerr << "\tFailed to setup event " << event["name"] << std::endl;
            } else {
                m_event_names.push_back(event["name"]);
            }
        }
        return *this;
    }

    int add_event(const json &event, int &fd) {
        auto event_type = event["type"] == "PERF_TYPE_HARDWARE" ? PERF_TYPE_HARDWARE : PERF_TYPE_RAW;
        auto pea = setup_pea(event_type, event["id"]);
        event_config new_event;
        new_event.fd = syscall(__NR_perf_event_open, &pea, 0, -1, fd, 0);
        if (new_event.fd <= 0) {
            return -1;
        } else {
            int32_t ret = ioctl(new_event.fd, PERF_EVENT_IOC_ID, &new_event.id);
            assert(ret == 0);

            if (fd == -1) {
                fd = new_event.fd;
            }

            return 0;
        }
    }

    perf_event_attr setup_pea(uint32_t type, uint32_t config) {
        struct perf_event_attr pea;
        std::memset(&pea, 0, sizeof(struct perf_event_attr));
        pea.type = type;
        pea.size = sizeof(perf_event_attr);
        pea.config = config;
        pea.inherit = 1;
        pea.exclude_kernel = 1;
        pea.exclude_hv = 0;
        pea.read_format = PERF_FORMAT_GROUP | PERF_FORMAT_ID;
        return pea;
    }

    std::map<std::string, uint64_t> get_events() {
        std::map<std::string, uint64_t> ret;
        for (uint32_t i = 0; i < m_event_names.size(); ++i) {
            const auto &event_name = m_event_names[i];
            const uint64_t value = m_data.counters[i].value;
            ret.insert({event_name, value});
        }
        return ret;
    }

};




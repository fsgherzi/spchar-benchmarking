//
// Created by Francesco Sgherzi on 22/08/21.
//

#pragma once

#include <vector>
#include <chrono>
#include <string>
#include <KokkosSparse_CrsMatrix.hpp>
#include <KokkosKernels_default_types.hpp>
#include <KokkosKernels_IOUtils.hpp>
#include <KokkosSparse_spadd.hpp>
#include <KokkosSparse_spgemm.hpp>
#include <Kokkos_Core.hpp>

#include "config.h"
#include "perf_profiler.h"
#if defined(USE_ARMPL)
#include "armpl.h"
#endif

enum MMComputationType {
    Symbolic,
    Numeric
};
using IProfiler = ProfilerInstance<PerfProfiler<std::chrono::nanoseconds>>;

using u32 = uint32_t;
using u64 = uint64_t;
using i32 = int;
using f32 = float;
using f64 = double;

using Scalar  = f32;
using Ordinal = i32;
using Offset  = i32;
using Layout  = default_layout;

using device_type = typename Kokkos::Device<Kokkos::DefaultExecutionSpace, typename Kokkos::DefaultExecutionSpace::memory_space>;
using execution_space = typename device_type::execution_space;
using memory_space    = typename device_type::memory_space;
using matrix_type = typename KokkosSparse::CrsMatrix<Scalar, Ordinal, device_type, void, Offset>;
#if defined(USE_ARMPL)
using armpl_matrix_type = armpl_spmat_t; 
#endif
using KernelHandle = KokkosKernels::Experimental::KokkosKernelsHandle<const Offset, const Ordinal, const Scalar, execution_space, memory_space, memory_space>;
using system_clock = std::chrono::high_resolution_clock;
using scalar_view_t = typename matrix_type::values_type::non_const_type;
using lno_view_t = typename matrix_type::row_map_type::non_const_type;
using lno_nnz_view_t = typename matrix_type::index_type::non_const_type;
using idx_t = typename lno_nnz_view_t::value_type;
using size_type = Offset;


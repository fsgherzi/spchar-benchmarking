# Kokkos Benchmarks

## Installation
Requirement: 
* CMake 3.16+
* Boost 1.58.0+
* Kokkos 3.5.0+
* Kokkos Kernels 3.5.0+
* Python 3.6.0+

To begin installation, create the build directory: 
```bash
mkdir build
cd build
```

enabling perf and performance counters for a specified architecture require the editing of `CMakeLists.txt`.
To enable perf profiling, add to `CMAKE_CXX_FLAGS` `-DUSE_PERF`.
To enable the performance counters add to `CMAKE_CXX_FLAGS` one of the following lines: 
* Fujitsu a64fx -> `A64FX`
* Kunpeng 920 -> `KUNPENG`
* Graviton3 -> `ARM64`
* Generic Intel -> `X86_64`

run `cmake` and `make` the project

```bash
cmake .. -DCMAKE_CXX_COMPILER=`which g++` \
-DCMAKE_PREFIX_PATH=<library_path> 
make -j`nproc`
```

Some options for kokkos can be forwarded as follows: 
```bash
cmake .. -DCMAKE_CXX_COMPILER=`which g++` \
-DCMAKE_PREFIX_PATH=<library_path> \
-DKokkos_USE_OPENMP=On
```
although options for architecture specific optimizations (`Kokkos_ARCH_A64FX` or `Kokkos_ENABLE_AGGRESSIVE_VECTORIZATION`) must be specified during kokkos' build.

## Running the Benchmarks
First, export the directory containing the matrices in sparse format: 
```bash
export MATRIX_DIR=<directory containing .mtx files>
```

Then, run the helper script: 
```bash
python runall.py
```

By default, all benchmarks (SpMV, SpADD-{Symbolic, Numeric}, SpGEMM-{Symbolic, Numeric}) are run on all the matrices using the maximum number of hardware threads available in the system.
